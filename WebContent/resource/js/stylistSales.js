jQuery(function ($) {
	var typeParam = "";
	var nameParam = "";
	var model = {};
	var App = {
			init: function () {
				this.makeCache();
				this.load();
				this.bind();
			},

			makeCache: function(){
				model["revenue-chart"] = $("#revenue-chart");
			},

			load: function(){
				this.initMenu();
				this.initSelectBox();
				this.initGlaphData();
				this.inisStylistData();
			},

			initMenu: function(){
				$($(".sidebar-menu li.treeview")[0]).addClass("active");
				$($(".sidebar-menu ul.treeview-menu")[0]).addClass("menu-open").attr("style","display: block;");
				$($(".sidebar-menu ul.treeview-menu li")[1]).addClass("selected");
			},


			initSelectBox :function(){
				CurrentDate = new Date();
				var thisYear = CurrentDate.getFullYear();
				var targetYear = thisYear;// month = 1の時用

				var fromMonthSelectBox = $("#from_month");
				var toMonthSelectBox = $("#to_month");
				var thisMonth = CurrentDate.getMonth() + 1;
				var targetMonth;
				if(thisMonth == 1){
					targetMonth = 12
					targetYear = targetYear -1;
				}else{
					targetMonth = thisMonth - 1;
				}

				for ( var j = 1; j < 13; j++) {
					fromMonthSelectBox.append($("<option>").val(j).text(j));
					toMonthSelectBox.append($("<option>").val(j).text(j));
				}

				fromMonthSelectBox.find("option").each(function(){
					if($(this).val() == targetMonth){
						$(this).attr("selected","selected");
					}
				});
				toMonthSelectBox.find("option").each(function(){
					if($(this).val() == targetMonth){
						$(this).attr("selected","selected");
					}
				});

				var fromYearSelectBox = $("#from_year");
				var toYearSelectBox = $("#to_year");
				var fromYear = 2010;
				for ( var i = fromYear; i <= thisYear; i++) {
					if(i == targetYear){
						fromYearSelectBox.append($("<option>").attr("selected","selected").val(i).text(i));
						toYearSelectBox.append($("<option>").attr("selected","selected").val(i).text(i));
					}else{
						fromYearSelectBox.append($("<option>").val(i).text(i));
						toYearSelectBox.append($("<option>").val(i).text(i));
					}
				}
			},

			initGlaphData: function(){
				var area = new Morris.Area({
					element: 'revenue-chart',
					resize: true,
					data:[],
					xkey: 'term',
					ykeys: 'monthSales',
					labels: ['月別売上'],
					lineColors: '#a0d0e0',
					hideHover: 'auto'
				});
			},

			inisStylistData: function(){
				$.ajax({
					type: "post",
					url: "./analyze",
					data: {type: 'api',name : 'load_stylist_list'},
					dataType: "json",
					success: function(data){
						var stylistSel = $("#stylist_sel");
						$.each(data, function(i){
							stylistSel.append("<option value='" + data[i] + "'>" + data[i] + "</option>");
						});
					}
				});
			},

			bind: function(){
				this.dateCheck();
			},

			dateCheck: function(){
				$("#display_sales").bind("click",function(){
					var fromYear = $('[name=from_year] option:selected').text();
					var fromMonth = $('[name=from_month] option:selected').text();
					var toYear = $('[name=to_year] option:selected').text();
					var toMonth = $('[name=to_month] option:selected').text();

					if(fromMonth.length == 1){
						termStart = fromYear + "0" + fromMonth + "00";// for 月初
					}else{
						termStart = fromYear + fromMonth + "00";// for 月初
					}
					if(toMonth.length == 1){
						termEnd = toYear + "0" + toMonth + "31";// for 月初
					}else{
						termEnd = toYear + toMonth + "31";// for 月初
					}

					var stylist = $("#stylist_sel option:selected").text();
					if(Number(termStart) > Number(termEnd)){
						alert("検索期間を正しく設定して下さい");

					}else if(stylist == null){
						alert("スタイリストを選択して下さい");
					}else{
						App.clickDisplayButton(termStart, termEnd, stylist);
					}
				});
			},

			clickDisplayButton : function(termStart, termEnd, stylist){
				var json = JSON.stringify({"termStart": Number(termStart), "termEnd":Number(termEnd), "stylist": stylist});
				$("#revenue-chart").html("<img src='./resource/img/loading.gif' width='80' height='80'" +
				"alt='Now Loading...' style='display: block;margin:75px auto'/>")
				$.ajax({
					type: "post",
					url: "./analyze",
					data: {type: 'api',name : 'load_sales_stylist', data : json},
					dataType: "json",
					success: function(data){
						var area = new Morris.Area({
							element: 'revenue-chart',
							resize: true,
							data:data,
							xkey: 'term',
							ykeys: ['monthlySales'],
							labels: ['月間売上'],
							lineColors: ['#a0d0e0'],
							hideHover: true,
							hoverCallback: function (index, options, content) {
								var row = options.data[index];

								$("#sales_rate_term").text("[" + row.term + "]における顧客、売上分析");

								var sales_donut = new Morris.Donut({
									element: 'sales_rate_chart',
									resize: true,
									colors: ["#68be8d", "#89c3eb", "#fcc800"],
									data: [
									       {label: "指名売上", value: row.nominationSales},
									       {label: "フリー売上", value: row.freeSales},
									       {label: "店販", value: row.goodsSales}
									       ],
									       hideHover: 'auto'
								});
								$("#gender_rate_term").text("[" + row.term + "]における売上内訳");
								$("#total_price").text(row.monthlySales.toLocaleString() + "円");
								$("#nomination_sales").text(row.nominationSales.toLocaleString() + "円");
								$("#free_sales").text(row.freeSales.toLocaleString() + "円");
								$("#store_sales").text(row.goodsSales.toLocaleString() + "円");
								$("#unit_price").text(Math.floor(row.monthlySales / row.totalCount).toLocaleString() + "円");

								var gender_donut = new Morris.Donut({
									element: 'gender_rate_chart',
									resize: true,
									colors: ["#3c8dbc", "#f56954", "#00a65a"],
									data: [
									       {label: "男性", value: row.manCount},
									       {label: "女性", value: row.womanCount},
									       {label: "不明", value: row.totalCount - row.manCount - row.womanCount}
									       ],
									       hideHover: 'auto'
								});
								$("#total_customer_count").text(row.totalCount + "名様");
								$("#newer_repeater").text("新規：" + row.newer + "人, リピーター：" + row.repeater + "人");
								$("#gender_sales_rate").text("男性：" + row.manCount + "人, 女性：" + row.womanCount + "人, 不明" + (row.totalCount - row.manCount - row.womanCount) + "人");
								var ageAverage = row.ageAverage == 0 ? "担当したお客様の誕生日が登録されていません" : row.ageAverage + "歳";
								$("#age_average").text(ageAverage);


								return '<div class="hover-title">' + row.term
								+ '</div><b style="color: white">'
								+ " </b><span>" + options.labels[0] + " : " + Number(row.monthlySales).toLocaleString() + "</span>";
							}
						});
					},
				});
			}
	}

	App.init();
});