jQuery(function ($) {
    var typeParam = "";
    var nameParam = "";
    var model = {};
    var App = {
        init: function () {
            this.makeCache();
            this.load().done(this.bind);
        },

        makeCache: function(){
        	// ------ ToDo list ------
        	model["todo_pagenation"] = $("#todo_pagenation");
//              <li><a href="#">1</a></li>
        	model["todo_list"] = $("#todo_list");
        	model["todo_list_data_cache"] = {};
        },

        load: function(){
        	var dfd = $.Deferred();
        	$.when(this.loadToDoData(), this.loadSalesData()).done(function(){
        		dfd.resolve();
        	});
    		return dfd.promise();
        },

        loadToDoData: function(){
        	var dfd = $.Deferred();
        	$.ajax({
        		  type: "post",
        		  url: "./analyze",
        		  data: {type: 'api',name : 'load_todo_data'},
        		  dataType: "json",
        		  success: function(data){
        				$.each(data, function(i){
        					model["todo_list_data_cache"][data[i].historyId] = data[i];
        					var checked = data[i].attacked == 1 ? "checked='checked'" : "";
        					model["todo_list"].append("<li><input type='checkbox' value='' name='" + data[i].historyId + "' " + checked + "/><span class='text'>[" + data[i].stylist + "] : " + data[i].nameKanji + " / " + data[i].nameKana + "</span></li>");
        				});
        				dfd.resolve();
        		  }
        		});
        	return dfd.promise();
        },

        loadSalesData: function(){
        	var dfd = $.Deferred();
			$("#revenue-chart").html("<img src='./resource/img/loading.gif' width='80' height='80'" +
			"alt='Now Loading...' style='display: block;margin:75px auto'/>");

        	$.ajax({
      		  type: "post",
      		  url: "./analyze",
      		  data: {type: 'api',name : 'load_sales_compare'},
      		  dataType: "json",
      		  success: function(salesData){
              	var area = new Morris.Area({
            	    element: 'revenue-chart',
            	    resize: true,
            	    data:salesData,
            	    xkey: 'term',
            	    ykeys: ['thisYearSales', 'lastYearSales'],
            	    labels: ['今年度', '昨年度'],
            	    lineColors: ['#a0d0e0', '#3c8dbc'],
            	    hideHover: 'auto'
            	  });
      			  dfd.resolve();
      		  }
      		});
        	return dfd.promise();
        },

        bind: function(){
        	var dfd = $.Deferred();
        	$.when(App.toDoListCheckBind()).done(function(){
        		dfd.resolve();
        	});
        	return dfd.promise();

        },

        toDoListCheckBind: function(){
        	model["todo_list"].find("input").change(function(){
        		var status = 0;
        		if($(this).is(':checked')){
        			status = 1;
        		}
        		model["todo_list_data_cache"][Number(this["name"])]["status"] = status;
        		var json = JSON.stringify(model["todo_list_data_cache"][Number(this["name"])])
            	$.ajax({
          		  type: "post",
          		  url: "./analyze",
          		  data: {type: 'api',name : 'check_todo', data : json},
          		  dataType: "json",
          		  success: function(){
          		  }
          		});
        	});
        },
        mainMenuBind : function(){

        }
    }

    App.init();
});