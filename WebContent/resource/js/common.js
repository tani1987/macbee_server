jQuery(function ($) {
	var typeParam = "";
	var nameParam = "";
	var model = {};
	var App = {
			init: function () {
				this.makeCache();
				this.load().done(this.bind);
			},

			makeCache: function(){
			},

			load: function(){
				var dfd = $.Deferred();
				$.when(this.loadHint()).done(function(){
					dfd.resolve();
				});
				return dfd.promise();
			},

			loadHint: function(){
				var dfd = $.Deferred();
				$("[data-toggle=tooltip]").tooltip();
				dfd.resolve();
				return dfd.promise();
			},


			bind: function(){
				var dfd = $.Deferred();
				$.when(App.toDoListCheckBind(),App.slideMenu()).done(function(){
					dfd.resolve();
				});
				return dfd.promise();

			},

			toDoListCheckBind: function(){
				var dfd = $.Deferred();
				dfd.resolve();
				return dfd.promise();
			},

			slideMenu: function(){
				var dfd = $.Deferred();
				$("#menu_config").bind("click", function(){
					var offset = $(".main-sidebar").offset();
					if(offset.left < 0){
						$(".main-sidebar").css("transform","translate(0, 0)");
					}else{
						$(".main-sidebar").css("transform","translate(-230px, 0)");
					}
				});
				dfd.resolve();
				return dfd.promise();
			}
	}

	App.init();
});