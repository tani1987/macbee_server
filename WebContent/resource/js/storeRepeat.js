jQuery(function ($) {
	var typeParam = "";
	var nameParam = "";
	var model = {};
	var App = {
			init: function () {
				this.makeCache();
				this.load();
				this.bind();
			},

			makeCache: function(){
				model["repeat_table_tbody"] = $("#repeat_table tbody");
			},

			load: function(){
				this.initSelectBox();
				this.initMenu();
			},

			initMenu: function(){
				$($(".sidebar-menu li.treeview")[2]).addClass("active");
				$($(".sidebar-menu ul.treeview-menu")[2]).addClass("menu-open").attr("style","display: block;");
				$($(".sidebar-menu ul.treeview-menu li")[4]).addClass("selected");
			},

			initSelectBox :function(){
				CurrentDate = new Date();
				var thisYear = CurrentDate.getFullYear();
				var targetYear = thisYear;// month = 1の時用

				var firstComingFromMonthSelectBox = $("#first_coming_from_month");
				var firstComingToMonthSelectBox = $("#first_coming_to_month");
				var repeatFromMonthSelectBox = $("#repeat_from_month");
				var repeatToMonthSelectBox = $("#repeat_to_month");

				var thisMonth = CurrentDate.getMonth() + 1;
				var targetMonth;
				if(thisMonth == 1){
					targetMonth = 12
					targetYear = targetYear -1;
				}else{
					targetMonth = thisMonth - 1;
				}

				for ( var j = 1; j < 13; j++) {
					firstComingFromMonthSelectBox.append($("<option>").val(j).text(j));
					firstComingToMonthSelectBox.append($("<option>").val(j).text(j));
					repeatFromMonthSelectBox.append($("<option>").val(j).text(j));
					repeatToMonthSelectBox.append($("<option>").val(j).text(j));
				}

				firstComingFromMonthSelectBox.find("option").each(function(){
					if($(this).val() == targetMonth){
						$(this).attr("selected","selected");
					}
				});
				firstComingToMonthSelectBox.find("option").each(function(){
					if($(this).val() == targetMonth){
						$(this).attr("selected","selected");
					}
				});
				repeatFromMonthSelectBox.find("option").each(function(){
					if($(this).val() == targetMonth){
						$(this).attr("selected","selected");
					}
				});
				repeatToMonthSelectBox.find("option").each(function(){
					if($(this).val() == thisMonth){
						$(this).attr("selected","selected");
					}
				});

				var firstComingFromYearSelectBox = $("#first_coming_from_year");
				var firstComingToYearSelectBox = $("#first_coming_to_year");
				var repeatFromYearSelectBox = $("#repeat_from_year");
				var repeatToYearSelectBox = $("#repeat_to_year");
				var fromYear = 2010;

				for ( var i = fromYear; i <= thisYear; i++) {
					if(i == targetYear){
						firstComingFromYearSelectBox.append($("<option>").attr("selected","selected").val(i).text(i));
						firstComingToYearSelectBox.append($("<option>").attr("selected","selected").val(i).text(i));
						repeatFromYearSelectBox.append($("<option>").attr("selected","selected").val(i).text(i));
						if(thisMonth == 1){
							repeatToYearSelectBox.append($("<option>").val(i).text(i));
						}else{
							repeatToYearSelectBox.append($("<option>").attr("selected","selected").val(i).text(i));
						}
					}else{
						firstComingFromYearSelectBox.append($("<option>").val(i).text(i));
						firstComingToYearSelectBox.append($("<option>").val(i).text(i));
						repeatFromYearSelectBox.append($("<option>").val(i).text(i));
						if(thisMonth == 1 && i == targetYear + 1){
							repeatToYearSelectBox.append($("<option>").attr("selected","selected").val(i + 1).text(i +1));
						}else{
							repeatToYearSelectBox.append($("<option>").val(i).text(i));
						}

					}
				}
			},

			bind: function(){
				this.dateCheck();
				this.connectSelectBoxes();
			},

			dateCheck: function(){
				$("#display_sales").bind("click",function(){
					var firstComingFromYear = $('[name=first_coming_from_year] option:selected').text();
					var firstComingFromMonth = $('[name=first_coming_from_month] option:selected').text();
					var firstComingToYear = $('[name=first_coming_to_year] option:selected').text();
					var firstComingToMonth = $('[name=first_coming_to_month] option:selected').text();
					var firstComingTermStart = 0;
					var firstComingTermEnd = 0;
					if(firstComingFromMonth.length == 1){
						firstComingTermStart = firstComingFromYear + "0" + firstComingFromMonth + "00";// for 月初
					}else{
						firstComingTermStart = firstComingFromYear + firstComingFromMonth + "00";// for 月初
					}
					if(firstComingToMonth.length == 1){
						firstComingTermEnd = firstComingToYear + "0" + firstComingToMonth + "31";// for 月初
					}else{
						firstComingTermEnd = firstComingToYear + firstComingToMonth + "31";// for 月初
					}

					var repeatFromYear = $('[name=repeat_from_year] option:selected').text();
					var repeatFromMonth = $('[name=repeat_from_month] option:selected').text();
					var repeatToYear = $('[name=repeat_to_year] option:selected').text();
					var repeatToMonth = $('[name=repeat_to_month] option:selected').text();
					var repeatTermStart = 0;
					var repeatTermEnd = 0;
					if(repeatFromMonth.length == 1){
						repeatTermStart = repeatFromYear + "0" + repeatFromMonth + "00";// for 月初
					}else{
						repeatTermStart = repeatFromYear + repeatFromMonth + "00";// for 月初
					}
					if(repeatToMonth.length == 1){
						repeatTermEnd = repeatToYear + "0" + repeatToMonth + "31";// for 月初
					}else{
						repeatTermEnd = repeatToYear + repeatToMonth + "31";// for 月初
					}

					if(Number(firstComingTermStart) > Number(firstComingTermEnd)){
						alert("初回来店日の期間を正しく指定して下さい");
					}else if(Number(repeatTermStart) > Number(repeatTermEnd)){
						alert("リピート率の算出期間を正しく指定して下さい");
					}else{
						App.clickDisplayButton(firstComingTermStart, firstComingTermEnd, repeatTermStart, repeatTermEnd);
					}
				});
			},

			connectSelectBoxes : function(){
				$("#first_coming_from_year").bind("change", function(){
					var keyString = "#repeat_from_year [value=" + $(this).val() + "]";
					$(keyString).attr("selected","selected");
				});
				$("#first_coming_from_month").bind("change", function(){
					var keyString = "#repeat_from_month [value=" + $(this).val() + "]";
					$(keyString).attr("selected","selected");
				});
			},

			clickDisplayButton : function(firstComingTermStart, firstComingTermEnd, repeatTermStart, repeatTermEnd){
				$("#repeat_table tbody").find("tr").remove();
				$("#totalRow").remove();
				$("#repeat_table").append("<img src='./resource/img/loading.gif' width='80' height='80'" +
				"alt='Now Loading...' style='display: block;margin:75px auto'/>")

				var json = JSON.stringify({"firstComingTermStart": Number(firstComingTermStart), "firstComingTermEnd":Number(firstComingTermEnd)
					, "repeatTermStart": Number(repeatTermStart), "repeatTermEnd":Number(repeatTermEnd)});
				$.ajax({
					type: "post",
					url: "./analyze",
					data: {type: 'api',name : 'load_coming_store', data : json},
					dataType: "json",
					success: function(data){
						var currentMonth = "";
						var first = 0;
						var second = 0;
						var third = 0;
						var fourth = 0;
						var fifth = 0;
						var sixth = 0;
						var seventh = 0;
						var eighth = 0;
						var totalMonthCount = 0;
						var totalFirst = 0;
						var totalSecond = 0;
						var totalThird = 0;
						var totalFourth = 0;
						var totalFifth = 0;
						var totalSixth = 0;
						var totalSeventh = 0;
						var totalEighth = 0;
						var totalTermCount = 0;
						var yearAndMonth = firstComingTermStart.split("/");

						if(data.length == 0){
							model["repeat_table_tbody"].append("<tr><td>" + yearAndMonth[0] + "年</td><td>" + yearAndMonth[1] + "月</td><td>"
									// + first +"人</td><td>" + Number((first/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ totalMonthCount +"人</td><td>"
									+ second +"人</td><td>" + Number((second/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ third +"人</td><td>" + Number((third/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ fourth +"人</td><td>" + Number((fourth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ fifth +"人</td><td>" + Number((fifth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ sixth +"人</td><td>" + Number((sixth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ seventh +"人</td><td>" + Number((seventh/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ eighth +"人</td><td>" + Number((eighth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
							);
							$("#repeat_table").find("img").remove()
						}else{
							for ( var i = 0; i < data.length; i++) {
								if(currentMonth == data[i].first_coming){
									switch(data[i].coming_times){
									case "1":
										first++;
										totalFirst++;
										break;
									case "2":
										second++;
										totalSecond++;
										break;
									case "3":
										second++;
										third++;
										totalSecond++;
										totalThird++;
										break;
									case "4":
										second++;
										third++;
										fourth++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										break;
									case "5":
										second++;
										third++;
										fourth++;
										fifth++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										totalFifth++;
										break;
									case "6":
										second++;
										third++;
										fourth++;
										fifth++;
										sixth++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										totalFifth++;
										totalSixth++;
										break;
									case "7":
										second++;
										third++;
										fourth++;
										fifth++;
										sixth++;
										seventh++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										totalFifth++;
										totalSixth++;
										totalSeventh++;
										break;
									case undefined:
										break;
									default:
										second++;
									third++;
									fourth++;
									fifth++;
									sixth++;
									seventh++;
									eighth++;
									totalSecond++;
									totalThird++;
									totalFourth++;
									totalFifth++;
									totalSixth++;
									totalSeventh++;
									totalEighth++;
									break;
									}
									totalMonthCount++;
									totalTermCount++;
								}else{
									if(currentMonth != ""){
										yearAndMonth = currentMonth.split("/");
										model["repeat_table_tbody"].append("<tr><td>" + yearAndMonth[0] + "年</td><td>" + yearAndMonth[1] + "月</td><td>"
												// + first +"人</td><td>" + Number((first/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ totalMonthCount +"人</td><td>"
												+ second +"人</td><td>" + Number((second/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ third +"人</td><td>" + Number((third/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ fourth +"人</td><td>" + Number((fourth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ fifth +"人</td><td>" + Number((fifth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ sixth +"人</td><td>" + Number((sixth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ seventh +"人</td><td>" + Number((seventh/totalMonthCount)*100).toFixed(1) +"％</td><td>"
												+ eighth +"人</td><td>" + Number((eighth/totalMonthCount)*100).toFixed(1) +"％</td><tr>"
										);
									}else{
										$("#repeat_table").find("img").remove()
									}

									currentMonth = data[i].first_coming;
									yearAndMonth = currentMonth.split("/");
									first = 0;
									second = 0;
									third = 0;
									fourth = 0;
									fifth = 0;
									sixth = 0;
									seventh = 0;
									eighth = 0;
									totalMonthCount = 0;
									switch(data[i].coming_times){
									case "1":
										first++;
										totalFirst++;
										break;
									case "2":
										second++;
										totalSecond++;
										break;
									case "3":
										second++;
										third++;
										totalSecond++;
										totalThird++;
										break;
									case "4":
										second++;
										third++;
										fourth++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										break;
									case "5":
										second++;
										third++;
										fourth++;
										fifth++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										totalFifth++;
										break;
									case "6":
										second++;
										third++;
										fourth++;
										fifth++;
										sixth++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										totalFifth++;
										totalSixth++;
										break;
									case "7":
										second++;
										third++;
										fourth++;
										fifth++;
										sixth++;
										seventh++;
										totalSecond++;
										totalThird++;
										totalFourth++;
										totalFifth++;
										totalSixth++;
										totalSeventh++;
										break;
									case undefined:
										break;
									default:
										second++;
									third++;
									fourth++;
									fifth++;
									sixth++;
									seventh++;
									eighth++;
									totalSecond++;
									totalThird++;
									totalFourth++;
									totalFifth++;
									totalSixth++;
									totalSeventh++;
									totalEighth++;
									break;
									}
									totalMonthCount++;
									totalTermCount++;
								}
							}
							model["repeat_table_tbody"].append("<tr><td>" + yearAndMonth[0] + "年</td><td>" + yearAndMonth[1] + "月</td><td>"
									// + first +"人</td><td>" + Number((first/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ totalMonthCount +"人</td><td>"
									+ second +"人</td><td>" + Number((second/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ third +"人</td><td>" + Number((third/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ fourth +"人</td><td>" + Number((fourth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ fifth +"人</td><td>" + Number((fifth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ sixth +"人</td><td>" + Number((sixth/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ seventh +"人</td><td>" + Number((seventh/totalMonthCount)*100).toFixed(1) +"％</td><td>"
									+ eighth +"人</td><td>" + Number((eighth/totalMonthCount)*100).toFixed(1) +"％</td><tr>"
							);

							// 一番上に合計行の挿入
							$("#repeat_table thead").after("<tr id='totalRow'><td colspan='2'>期間合計</td><td>"
									+ totalTermCount +"人</td><td>"
									+ totalSecond +"人</td><td>" + Number((totalSecond/totalTermCount)*100).toFixed(1) +"％</td><td>"
									+ totalThird +"人</td><td>" + Number((totalThird/totalTermCount)*100).toFixed(1) +"％</td><td>"
									+ totalFourth +"人</td><td>" + Number((totalFourth/totalTermCount)*100).toFixed(1) +"％</td><td>"
									+ totalFifth +"人</td><td>" + Number((totalFifth/totalTermCount)*100).toFixed(1) +"％</td><td>"
									+ totalSixth +"人</td><td>" + Number((totalSixth/totalTermCount)*100).toFixed(1) +"％</td><td>"
									+ totalSeventh +"人</td><td>" + Number((totalSeventh/totalTermCount)*100).toFixed(1) +"％</td><td>"
									+ totalEighth +"人</td><td>" + Number((totalEighth/totalTermCount)*100).toFixed(1) +"％</td><tr>"
							);
						}
					}
				});
			}
	}

	App.init();
});