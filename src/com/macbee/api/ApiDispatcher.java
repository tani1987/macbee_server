package com.macbee.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ApiDispatcher {


	public void dispatch(HttpServletRequest req, HttpServletResponse res)
			throws Exception {

		String key =  req.getParameter("name");
		if(req.getSession() == null){
			return;
		}
		IApi api = getApi(key);
		api.execute(req, res);
	}

	private IApi getApi(String key) {
		IApi api = null;
		for (ApiEnum apiEnum : ApiEnum.values()) {
			if(apiEnum.name().equalsIgnoreCase(key)){
				api = apiEnum.getApi();
			}
		}
		return api;
	}
}
