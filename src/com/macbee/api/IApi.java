package com.macbee.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IApi {
	void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception;
}
