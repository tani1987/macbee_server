package com.macbee.api.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.macbee.api.AbsApi;
import com.macbee.dao.LoadComingStoreDao;
import com.macbee.dao.LoadComingStylistDao;
import com.macbee.dao.LoadToDoListDao;
import com.macbee.model.ToDoListModel;

public class LoadComingStylistApi extends AbsApi {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		int userId = getUserIdFromSession(req);
		Map<String, String> data = makeObjFromJson(req.getParameter("data"), HashMap.class);
		int firstComingTermStart = makeIntFromJsonNum(data.get("firstComingTermStart"));
		int firstComingTermEnd = makeIntFromJsonNum(data.get("firstComingTermEnd"));
		int repeatTermStart = makeIntFromJsonNum(data.get("repeatTermStart"));
		int repeatTermEnd = makeIntFromJsonNum(data.get("repeatTermEnd"));
		String stylist = data.get("stylist");

		List<Map<String,String>> comingStoreList = new LoadComingStylistDao().loadData(userId, firstComingTermStart, firstComingTermEnd,repeatTermStart, repeatTermEnd, stylist);
		putJsonToRes(res, makeJson(comingStoreList));
	}

}
