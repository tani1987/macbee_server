package com.macbee.api.impl;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.macbee.api.AbsApi;
import com.macbee.dao.LoginDao;
import com.macbee.page.PageEnum;

public class LoginApi extends AbsApi {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		String userName = req.getParameter("user_name");
		String password = req.getParameter("password");
		int userId = 0;
		userId = new LoginDao().login(userName, password);
		if(userId != 0 ){
			HttpSession session = req.getSession(true);
			session.setAttribute("userId", userId);
			PageEnum.TOP.getPage().perform(req, res);
		}else{
			PageEnum.LOGIN_FAIL.getPage().perform(req, res);
		}
	}

}
