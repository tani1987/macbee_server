package com.macbee.api.impl;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.common.collect.Lists;
import com.macbee.api.AbsApi;
import com.macbee.dao.LoadSalesCompareDao;
import com.macbee.dao.LoadSalesStoreDao;
import com.macbee.dao.LoadToDoListDao;
import com.macbee.model.SalesCompareModel;
import com.macbee.model.SalesStoreModel;
import com.macbee.model.ToDoListModel;

public class LoadSalesStoreApi extends AbsApi {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		int userId = getUserIdFromSession(req);
		Map<String, String> data = makeObjFromJson(req.getParameter("data"), HashMap.class);
		int termStart = makeIntFromJsonNum(data.get("termStart"));
		int termEnd = makeIntFromJsonNum(data.get("termEnd"));


		List<SalesStoreModel> salesStoreModelList = Lists.newArrayList();
		salesStoreModelList = new LoadSalesStoreDao().loadData(userId, termStart, termEnd);
		putJsonToRes(res, makeJson(salesStoreModelList));
	}
}
