package com.macbee.api.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.macbee.api.AbsApi;
import com.macbee.dao.LoadStylistListDao;
import com.macbee.dao.LoadToDoListDao;
import com.macbee.model.ToDoListModel;

public class LoadStylistListApi extends AbsApi {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		int userId = getUserIdFromSession(req);
		List<String> stylistList = new LoadStylistListDao().loadData(userId);
		putJsonToRes(res, makeJson(stylistList));
	}

}
