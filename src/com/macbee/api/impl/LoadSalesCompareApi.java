package com.macbee.api.impl;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.common.collect.Lists;
import com.macbee.api.AbsApi;
import com.macbee.dao.LoadSalesCompareDao;
import com.macbee.dao.LoadToDoListDao;
import com.macbee.model.SalesCompareModel;
import com.macbee.model.ToDoListModel;

public class LoadSalesCompareApi extends AbsApi {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		int userId = getUserIdFromSession(req);

		Calendar currentCalendar = Calendar.getInstance();
		Calendar startMaonthCalendar = Calendar.getInstance();
		Calendar endMonthCalendar = Calendar.getInstance();

		startMaonthCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), 1, 0, 0, 0);

		endMonthCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), 1, 0, 0, 0);
		endMonthCalendar.add(Calendar.MONTH, 1);
		endMonthCalendar.set(Calendar.MILLISECOND, 0);
		endMonthCalendar.add(Calendar.MILLISECOND, -1);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dateSdf = new SimpleDateFormat("MMdd");
		startMaonthCalendar.add(Calendar.MONTH, -1);
		endMonthCalendar.add(Calendar.MONTH, -1);

		String thisYearStartDate = sdf.format(startMaonthCalendar.getTime());
		String thisYearEndDate = sdf.format(endMonthCalendar.getTime());

		// just for macbee demo
		startMaonthCalendar.add(Calendar.YEAR, -1);
		endMonthCalendar.add(Calendar.YEAR, -1);

		String lastYearStartDate = sdf.format(startMaonthCalendar.getTime());
		String lastYearEndDate = sdf.format(endMonthCalendar.getTime());


		List<Map<String, String>> thisYearDataList = new LoadSalesCompareDao().loadData(userId, Integer.parseInt(thisYearStartDate), Integer.parseInt(thisYearEndDate));
		List<Map<String, String>> lastYearDataList = new LoadSalesCompareDao().loadData(userId, Integer.parseInt(lastYearStartDate), Integer.parseInt(lastYearEndDate));

		List<SalesCompareModel> salesCompareModelList = Lists.newArrayList();

		if(thisYearDataList.size() > lastYearDataList.size()){
			for (int i = 0; i < thisYearDataList.size(); i++) {// 今年度の未来日に関してはグラフに何も表示しない
				SalesCompareModel model = new SalesCompareModel();
				String term = thisYearDataList.get(i).get("date");
				model.setTerm(term.substring(term.length()-4));
				model.setThisYearSales(Integer.parseInt(thisYearDataList.get(i).get("price")));
				if(lastYearDataList.size() > i){
					model.setLastYearSales(Integer.parseInt(lastYearDataList.get(i).get("price")));
				}
				salesCompareModelList.add(model);
			}
		}else{
			for (int i = 0; i < lastYearDataList.size(); i++) {// 今年度の未来日に関してはグラフに何も表示しない
				SalesCompareModel model = new SalesCompareModel();
				String term = lastYearDataList.get(i).get("date");
				model.setTerm(term.substring(term.length()-4));
				if(lastYearDataList.size() > i){
					model.setThisYearSales(Integer.parseInt(thisYearDataList.get(i).get("price")));
				}
				model.setLastYearSales(Integer.parseInt(lastYearDataList.get(i).get("price")));
				salesCompareModelList.add(model);
			}
		}
		String json = makeJson(salesCompareModelList);
		putJsonToRes(res, json);
	}
}
