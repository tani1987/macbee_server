package com.macbee.api.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.macbee.api.AbsApi;
import com.macbee.dao.CheckToDoDao;
import com.macbee.dao.LoginDao;
import com.macbee.page.PageEnum;

public class CheckToDoApi extends AbsApi {

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		int userId = getUserIdFromSession(req);
		Map<String, Object> data = makeObjFromJson(req.getParameter("data"), HashMap.class);
		new CheckToDoDao().updateStatus(makeIntFromJsonNum(data.get("status")), userId, makeIntFromJsonNum(data.get("historyId")));
	}


}
