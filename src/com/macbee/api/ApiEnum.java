package com.macbee.api;

import com.macbee.api.impl.CheckToDoApi;
import com.macbee.api.impl.LoadComingStoreApi;
import com.macbee.api.impl.LoadComingStylistApi;
import com.macbee.api.impl.LoadSalesCompareApi;
import com.macbee.api.impl.LoadSalesStoreApi;
import com.macbee.api.impl.LoadSalesStylistApi;
import com.macbee.api.impl.LoadStylistListApi;
import com.macbee.api.impl.LoadToDoListApi;
import com.macbee.api.impl.LoginApi;

public enum ApiEnum {
	LOGIN(new LoginApi()), LOAD_TODO_DATA(new LoadToDoListApi()),
	CHECK_TODO(new CheckToDoApi()),LOAD_SALES_COMPARE(new LoadSalesCompareApi())
	,LOAD_SALES_STORE(new LoadSalesStoreApi()),LOAD_STYLIST_LIST(new LoadStylistListApi())
	,LOAD_SALES_STYLIST(new LoadSalesStylistApi()),LOAD_COMING_STORE(new LoadComingStoreApi())
	,LOAD_COMING_STYLIST(new LoadComingStylistApi());
	private final IApi api;

	private ApiEnum(IApi api) {
		this.api = api;
	}

	public IApi getApi(){
		return this.api;
	}
}
