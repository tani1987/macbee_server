package com.macbee.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.arnx.jsonic.JSON;

public abstract class AbsApi implements IApi {

	public int getUserIdFromSession(HttpServletRequest req){
		return (int) req.getSession().getAttribute("userId");
	}

	public int makeIntFromJsonNum(Object obj ){
		return ((BigDecimal)obj).intValue();
	}

	public String makeJson(Object obj){
		return JSON.encode(obj);
	}

	@SuppressWarnings("unchecked")
	public <T> T makeObjFromJson(String json, Class<T> dummy){
		return JSON.decode(json, dummy);
	}

	public void putJsonToRes(HttpServletResponse res, String json) throws UnsupportedEncodingException, IOException{
		res.getOutputStream().write(json.getBytes("UTF-8"));
	}
}
