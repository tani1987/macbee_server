package com.macbee.base;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.macbee.api.ApiDispatcher;
import com.macbee.page.PageDispatcher;

public class RequestDispatcher {

	public void dispatch(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		try {
			req.setCharacterEncoding("UTF-8");
			res.setContentType("application/json; charset=UTF-8");

			String type = req.getParameter("type");
			if("api".equals(type)){
				new ApiDispatcher().dispatch(req, res);
			}else{
				new PageDispatcher().dispatch(req, res);
			}
		} catch(ClassNotFoundException e){
			System.out.println("開発メモ：プロジェクトのプロパティ＞");
			e.printStackTrace();
		} catch (Exception e) {
			if(!"sessionCheck".equals(req.getAttribute("status"))){
				e.printStackTrace();
			}
		}
	}
}
