package com.macbee.base;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.macbee.page.PageDispatcher;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/analyze")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String realPath;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
    }

    @Override
    public void init() throws ServletException{
    	super.init();
        this.realPath = this.getServletContext().getRealPath("/resource/");
    }

    public static String getRealPath(){
    	return realPath;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		new RequestDispatcher().dispatch(req, res);
	}

}
