package com.macbee.model;

public class ToDoListModel {

	private String nameKanji;
	private String nameKana;
	private String stylist;
	private int historyId;
 	private int attacked;// 0: not yet, 1: already done


	public String getNameKanji() {
		return nameKanji;
	}
	public void setNameKanji(String nameKanji) {
		this.nameKanji = nameKanji;
	}
	public String getNameKana() {
		return nameKana;
	}
	public void setNameKana(String nameKana) {
		this.nameKana = nameKana;
	}
	public String getStylist() {
		return stylist;
	}
	public void setStylist(String stylist) {
		this.stylist = stylist;
	}
	public int getAttacked() {
		return attacked;
	}
	public void setAttacked(int attacked) {
		this.attacked = attacked;
	}

	public int getHistoryId() {
		return historyId;
	}
	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}
}
