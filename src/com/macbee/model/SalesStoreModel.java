package com.macbee.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SalesStoreModel {
	private String term;
	private int nominationSales = 0;// 指名売上
	private int freeSales = 0;// フリー売上
	private int goodsSales = 0;// 店販
	private int newer = 0;
	private int repeater = 0;
	private int ageTotal = 0;
	private int ageCount = 0;
	private int avverageIntervel = 0;
	private int monthlySales = 0;
	private String genderRate;
	private int manCount = 0;
	private int womanCount = 0;
	private int totalCount = 0;


	public int getManCount() {
		return manCount;
	}
	public void setManCount(int manCount) {
		this.manCount = manCount;
	}
	public int getWomanCount() {
		return womanCount;
	}
	public void setWomanCount(int womanCount) {
		this.womanCount = womanCount;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public int getMonthlySales() {
		return monthlySales;
	}
	public void setMonthlySales(int monthlySales) {
		this.monthlySales = monthlySales;
	}
	public String getGenderRate() {
		return genderRate;
	}
	public void setGenderRate(String genderRate) {
		this.genderRate = genderRate;
	}


	public int getNominationSales() {
		return nominationSales;
	}
	public void setNominationSales(int nominationSales) {
		this.nominationSales = nominationSales;
	}
	public int getFreeSales() {
		return freeSales;
	}
	public void setFreeSales(int freeSales) {
		this.freeSales = freeSales;
	}
	public int getGoodsSales() {
		return goodsSales;
	}
	public void setGoodsSales(int goodsSales) {
		this.goodsSales = goodsSales;
	}
	public int getNewer() {
		return newer;
	}
	public void setNewer(int newer) {
		this.newer = newer;
	}
	public int getRepeater() {
		return repeater;
	}
	public void setRepeater(int repeater) {
		this.repeater = repeater;
	}
	public int getAgeTotal() {
		return ageTotal;
	}
	public void setAgeTotal(int ageTotal) {
		this.ageTotal = ageTotal;
	}
	public int getAgeAverage(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		int currentDate = Integer.parseInt(sdf.format(cal.getTime()));
		if(this.ageTotal == 0 || this.ageCount == 0){
			return 0;
		}
		String ageAverage = String.valueOf(currentDate - (this.ageTotal / this.ageCount));
		return Integer.parseInt(ageAverage.substring(0,2));
	}
	public int getAvverageIntervel() {
		return avverageIntervel;
	}
	public void setAvverageIntervel(int avverageIntervel) {
		this.avverageIntervel = avverageIntervel;
	}
	public int getAgeCount() {
		return ageCount;
	}
	public void setAgeCount(int ageCount) {
		this.ageCount = ageCount;
	}

}
