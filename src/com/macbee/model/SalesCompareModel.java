package com.macbee.model;

public class SalesCompareModel {
	private String  term;
	private int thisYearSales;
	private int lastYearSales;

	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public int getThisYearSales() {
		return thisYearSales;
	}
	public void setThisYearSales(int thisYearSales) {
		this.thisYearSales = thisYearSales;
	}
	public int getLastYearSales() {
		return lastYearSales;
	}
	public void setLastYearSales(int lastYearSales) {
		this.lastYearSales = lastYearSales;
	}
}
