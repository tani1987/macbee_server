package com.macbee.page;

import org.apache.commons.lang3.text.StrBuilder;

import com.macbee.base.MainServlet;
import com.macbee.page.impl.LoginAgainPage;
import com.macbee.page.impl.LoginFailPage;
import com.macbee.page.impl.LoginPage;
import com.macbee.page.impl.RfmAnalyzePage;
import com.macbee.page.impl.RfmConfigPage;
import com.macbee.page.impl.StoreRepeatPage;
import com.macbee.page.impl.StoreSalesPage;
import com.macbee.page.impl.StylistRepeatPage;
import com.macbee.page.impl.StylistSalesPage;
import com.macbee.page.impl.TopPage;


public enum PageEnum {
	COMMON_HEADER(null, "/html/common/commonHeader.html", null, null)
	, COMMON_TOOLBAR(null, "/html/common/commonToolbar.html", null, null)
	, COMMON_FOOTER(null, "/html/common/commonFooter.html", null, null)
	, COMMON_SCRIPT(null, "/html/common/commonScript.html", null, null)
	, COMMON_FLAME(null, "/html/common/commonFlame.html", null, null)
	, LOGIN(new LoginPage(),"/html/contents/login.html", "/js/login.js", "/css/login.css")
	, LOGIN_FAIL(new LoginFailPage(),"/html/contents/loginFail.html", null, null)
	, LOGIN_AGAIN(new LoginAgainPage(),"/html/contents/loginAgain.html", null, null)
	, TOP(new TopPage(),	"/html/contents/top.html", "/js/top.js", "/css/top.css")
	, STORE_SALES(new StoreSalesPage(),	"/html/contents/storeSales.html", "/js/storeSales.js", "/css/storeSales.css")
	, STYLIST_SALES(new StylistSalesPage(),	"/html/contents/stylistSales.html", "/js/stylistSales.js", "/css/stylistSales.css")
	, RFM_ANALYZE(new RfmAnalyzePage(),	"/html/contents/rfmAnalyze.html", "/js/rfmAnalyze.js", "/css/rfmAnalyze.css")
	, RFM_CONFIG(new RfmConfigPage(),	"/html/contents/rfmConfig.html", "/js/rfmConfig.js", "/css/rfmConfig.css")
	, STORE_REPEAT(new StoreRepeatPage(),	"/html/contents/storeRepeat.html", "/js/storeRepeat.js", "/css/storeRepeat.css")
	, STYLIST_REPEAT(new StylistRepeatPage(),	"/html/contents/stylistRepeat.html", "/js/stylistRepeat.js", "/css/stylistRepeat.css")
	;

	private final IPage page;
	private final String path;
	private final String extraScriptPath;
	private final String extraCssPath;


	//FIXME:ASAP
	private static final String BASE_PATH = "/WEB-INF/resource/";
//	private static final String BASE_PATH = "./WEB-INF/resource/html";

	private PageEnum(IPage page, String path,String extraScriptPath, String extraCssPath) {
		this.page = page;
		this.path = new StrBuilder(MainServlet.getRealPath()).append(path).toString();
		this.extraScriptPath = extraScriptPath;
		this.extraCssPath = extraCssPath;
	}

	public IPage getPage() {
		return this.page;
	}

	public String getPath() {
		return this.path;
	}

	public String getExtraScriptPath() {
		return this.extraScriptPath;
	}

	public String getExtraCssPath() {
		return this.extraCssPath;
	}

}
