package com.macbee.page;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.macbee.page.impl.LoginPage;



public class PageDispatcher {


	public void dispatch(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		res.setContentType("text/html;charset=UTF-8");

		try{
		String key = req.getParameter("name");
		IPage page = getPage(req.getSession(), key);
		page.perform(req, res);
		}catch(Exception e){
			dispachErrorPage(req,res);
			throw e;
		}
	}

	private IPage getPage(HttpSession session, String key) {
		IPage page = null;

		for (PageEnum pageEnum : PageEnum.values()) {
			if(pageEnum.name().equalsIgnoreCase(key)){
				page = pageEnum.getPage();
			}
		}

		if(page == null){
			page = PageEnum.LOGIN.getPage();
		}


		if(session == null && (!key.equalsIgnoreCase(PageEnum.LOGIN.name())
				&& !key.equalsIgnoreCase(PageEnum.LOGIN_FAIL.name()))){
			page = PageEnum.LOGIN_AGAIN.getPage();
		}
		return page;
	}


	private void dispachErrorPage(HttpServletRequest req,
			HttpServletResponse res) {
		try {
			IPage page = PageEnum.LOGIN_AGAIN.getPage();
			req.setAttribute("status", "sessionCheck");
			page.perform(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
