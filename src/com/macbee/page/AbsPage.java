package com.macbee.page;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.StrBuilder;

import static com.macbee.page.PageEnum.COMMON_FOOTER;
import static com.macbee.page.PageEnum.COMMON_HEADER;
import static com.macbee.page.PageEnum.COMMON_SCRIPT;
import static com.macbee.page.PageEnum.COMMON_TOOLBAR;
import static com.macbee.page.PageEnum.COMMON_FLAME;;


public abstract class AbsPage implements IPage {

	private static final String START_HTML = "<!DOCTYPE html>";
	private static final String END_HTML = "</html>";
	private static final String START_HEADER = "<head>";
	private static final String END_HEADER = "</head>";
	private static final String START_BODY = "<body class=\"skin-yellow-light sidebar-mini\"><div id=\"main\">";
	private static final String END_BODY = "</div></body>";

	public static final String START_JS = "<script src=\"./resource";
	public static final String END_JS = "\" type=\"text/javascript\"></script>";
	public static final String START_CSS = "<link href=\"./resource";
	public static final String END_CSS = "\" rel=\"stylesheet\" type=\"text/css\">";


	abstract protected void createBodyContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder);

	public void perform(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		checkSession(req);
		res.setContentType("text/html;charset=UTF-8");
		String html = createHtml(req, res);
		PrintWriter out = res.getWriter();
		out.println(html);
		out.close();
	}

	private void checkSession(HttpServletRequest req) throws Exception{
		String name = req.getParameter("name");
		if(name != null
				&& !PageEnum.LOGIN.name().equalsIgnoreCase(name)
				&& !PageEnum.LOGIN_FAIL.name().equalsIgnoreCase(name)
				&& !PageEnum.LOGIN_AGAIN.name().equalsIgnoreCase(name)){
			if(!"sessionCheck".equals(req.getAttribute("status"))){
				int test = (int) req.getSession().getAttribute("userId");
			}
		}
	}

	private String createHtml(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		StrBuilder htmlBuilder = new StrBuilder(START_HTML);
		createHeaderHtml(req, res, htmlBuilder);
		createBodyHtml(req, res, htmlBuilder);
		htmlBuilder.append(END_HTML);

		return htmlBuilder.toString();
	}

	private void createHeaderHtml(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		htmlBuilder.append(START_HEADER);
		createHeaderContents(req, res, htmlBuilder);
		addExtraCss(htmlBuilder);
		htmlBuilder.append(END_HEADER);
	}

	private void createBodyHtml(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		htmlBuilder.append(START_BODY);

		createFlameContents(req, res, htmlBuilder);

		createBodyContents(req, res, htmlBuilder);

		createFooterContents(req, res, htmlBuilder);

		createScriptContents(req, res, htmlBuilder);
		htmlBuilder.append(END_BODY);
	}

	protected void loadHtml(StrBuilder htmlBuilder, String htmlPath) {
		try {
			FileInputStream fis = new FileInputStream(htmlPath);
			InputStreamReader in = new InputStreamReader(fis, "UTF-8");
			BufferedReader br = new BufferedReader(in);
			String s;
			while ((s = br.readLine()) != null) {
				htmlBuilder.append(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed to load html file");
		}
	}

	/**
	 * You can override this method to create an individual header.
	 */
	protected void createHeaderContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		loadHtml(htmlBuilder, COMMON_HEADER.getPath());
	}

	/**
	 * You can add something by overriding if you need to add something to
	 * common css
	 */
	protected void addExtraCss(StrBuilder htmlBuilder) {
		// do nothing
	}


	/**
	 * You can override this method to create an individual footer.
	 */
	protected void createFooterContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		loadHtml(htmlBuilder, COMMON_FOOTER.getPath());
	}

	/**
	 * You can override this method to create an individual toolBar.
	 */
	protected void createFlameContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		// loadHtml(htmlBuilder, COMMON_TOOLBAR.getPath());
		loadHtml(htmlBuilder, COMMON_FLAME.getPath());
	}

	/**
	 * You can add something by overriding if you need to add something to
	 * common Footer
	 */
	protected void createScriptContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		loadHtml(htmlBuilder, COMMON_SCRIPT.getPath());
		addExtraScript(htmlBuilder);
	}

	/**
	 * You can add something by overriding if you need to add something to
	 * common Footer
	 */
	protected void addExtraScript(StrBuilder htmlBuilder) {
		// do nothing
	}
}