package com.macbee.page.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.StrBuilder;

import com.macbee.page.AbsPage;
import com.macbee.page.PageEnum;


public class RfmAnalyzePage extends AbsPage{
	@Override
	protected void createBodyContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		loadHtml(htmlBuilder, PageEnum.RFM_ANALYZE.getPath());
	}

	@Override
	protected void addExtraScript(StrBuilder htmlBuilder) {
		htmlBuilder.append(START_JS).append(PageEnum.RFM_ANALYZE.getExtraScriptPath()).append(END_JS);
	}

	@Override
	protected void addExtraCss(StrBuilder htmlBuilder) {
		htmlBuilder.append(START_CSS).append(PageEnum.RFM_ANALYZE.getExtraCssPath()).append(END_CSS);
	}

}
