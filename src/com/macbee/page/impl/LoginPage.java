package com.macbee.page.impl;

import static com.macbee.page.PageEnum.COMMON_FLAME;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.StrBuilder;

import com.macbee.page.AbsPage;
import com.macbee.page.PageEnum;


public class LoginPage extends AbsPage{
	@Override
	protected void createBodyContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		loadHtml(htmlBuilder, PageEnum.LOGIN.getPath());
	}

	@Override
	protected void addExtraScript(StrBuilder htmlBuilder) {
		htmlBuilder.append(START_JS).append(PageEnum.LOGIN.getExtraScriptPath()).append(END_JS);
	}

	@Override
	protected void addExtraCss(StrBuilder htmlBuilder) {
		htmlBuilder.append(START_CSS).append(PageEnum.LOGIN.getExtraCssPath()).append(END_CSS);
	}

	@Override
	protected void createFooterContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder){
		// hide footer
	}

	@Override
	protected void createFlameContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		// hide flame
	}
}
