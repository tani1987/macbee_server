package com.macbee.page.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.StrBuilder;

import com.macbee.page.AbsPage;
import com.macbee.page.PageEnum;


public class StoreRepeatPage extends AbsPage{
	@Override
	protected void createBodyContents(HttpServletRequest req,
			HttpServletResponse res, StrBuilder htmlBuilder) {
		loadHtml(htmlBuilder, PageEnum.STORE_REPEAT.getPath());
	}

	@Override
	protected void addExtraScript(StrBuilder htmlBuilder) {
		htmlBuilder.append(START_JS).append(PageEnum.STORE_REPEAT.getExtraScriptPath()).append(END_JS);
	}

	@Override
	protected void addExtraCss(StrBuilder htmlBuilder) {
		htmlBuilder.append(START_CSS).append(PageEnum.STORE_REPEAT.getExtraCssPath()).append(END_CSS);
	}

}
