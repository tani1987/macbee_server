package com.macbee.page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IPage {

	public void perform(HttpServletRequest req, HttpServletResponse res)
			throws Exception;
}
