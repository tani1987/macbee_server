package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.macbee.model.SalesCompareModel;
import com.macbee.model.ToDoListModel;
import com.macbee.util.DbManager;

public class LoadSalesCompareDao {

	private static final String SELECT_SQL = "SELECT accounting_date, SUM(price) FROM sales " +
			"WHERE user_id = ? " +
			"AND accounting_date BETWEEN ? AND ? GROUP BY accounting_date;";

	public List<Map<String, String>> loadData(int userId, int fromDate, int toDate) throws SQLException{
		List<Map<String, String>> modelList = Lists.newArrayList();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setInt(1, userId);
			ps.setInt(2,fromDate);
			ps.setInt(3,toDate);

			rs = ps.executeQuery();
			while(rs.next()){
				Map<String, String> data = Maps.newHashMap();
				data.put("date", rs.getString(1));
				data.put("price", rs.getString(2));
				modelList.add(data);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}

		return modelList;
	}
}
