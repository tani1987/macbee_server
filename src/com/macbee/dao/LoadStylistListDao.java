package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.google.common.collect.Lists;
import com.macbee.model.ToDoListModel;
import com.macbee.util.DbManager;

public class LoadStylistListDao {

	private static final String SELECT_SQL = "SELECT DISTINCT(stylist) FROM sales WHERE user_id = ? ORDER BY stylist DESC;";

	public List<String> loadData(int userId) throws SQLException{
		List<String> modelList = Lists.newArrayList();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setInt(1, userId);

			rs = ps.executeQuery();
			while(rs.next()){
				modelList.add(rs.getString(1));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}

		return modelList;
	}
}
