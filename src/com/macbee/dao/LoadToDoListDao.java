package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.google.common.collect.Lists;
import com.macbee.model.ToDoListModel;
import com.macbee.util.DbManager;

public class LoadToDoListDao {

	private static final String SELECT_SQL = "SELECT MIN(c.name_kanji), c.name_kana, s.stylist, h.history_id, h.attacked" +
	" FROM histories h, customers c, sales s" +
	" where h.customer_id = c.customer_id and h.user_id = c.user_id and h.user_id = s.user_id " +
	" and (c.name_kanji = s.name_kanji or c.name_kana = s.name_kana or c.name_kanji = s.name_kana or c.name_kana = s.name_kanji)" +
	" and h.user_id = ? and h.next_date > ? and  h.next_date < ?" +
	" GROUP BY c.name_kanji ORDER BY s.stylist;";

	private int daysRange = 10;

	public List<ToDoListModel> loadData(int userId) throws SQLException{
		List<ToDoListModel> modelList = Lists.newArrayList();

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		int baseDate = Integer.parseInt(sdf.format(calendar.getTime()));
		int fromDate = baseDate - this.daysRange;
		int toDate = baseDate + this.daysRange;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setInt(1, userId);
			ps.setInt(2,fromDate);
			ps.setInt(3,toDate);

			rs = ps.executeQuery();
			while(rs.next()){
				ToDoListModel model = new ToDoListModel();
				model.setNameKanji(rs.getString(1));
				model.setNameKana(rs.getString(2));
				model.setStylist(rs.getString(3));
				model.setHistoryId(rs.getInt(4));
				model.setAttacked(rs.getInt(5));
				modelList.add(model);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}

		return modelList;
	}
}
