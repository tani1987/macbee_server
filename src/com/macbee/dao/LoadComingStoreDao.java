package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.StrBuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.macbee.model.ToDoListModel;
import com.macbee.util.DbManager;

public class LoadComingStoreDao {

//	private static final String SELECT_SQL = "SELECT SUBSTRING(c.first_coming_store_date FROM 1 For 7), c.coming_store_times, h.coming_date"
			private static final String SELECT_SQL = "SELECT SUBSTRING(c.first_coming_store_date FROM 1 For 7), c.coming_store_times, h.accounting_date"
			+ " FROM customers c, histories h"
			+ " WHERE c.user_id = h.user_id AND c.customer_id = h.customer_id"
			+ " AND c.user_id = ? AND c.first_coming_store_date BETWEEN ? AND ? ORDER BY c.first_coming_store_date;";

	public List<Map<String, String>> loadData(int userId, int firstComingTermStart, int firstComingTermEnd,int repeatTermStart, int repeatTermEnd) throws SQLException{
		List<Map<String, String>> modelList = Lists.newArrayList();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		StrBuilder termStartStrBuilder = new StrBuilder(String.valueOf(firstComingTermStart));
		StrBuilder termEndDateStrBuilder = new StrBuilder(String.valueOf(firstComingTermEnd));
		termStartStrBuilder.insert(4, "/");
		termStartStrBuilder.insert(7, "/");
		termEndDateStrBuilder.insert(4, "/");
		termEndDateStrBuilder.insert(7, "/");

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setInt(1, userId);
			ps.setString(2,termStartStrBuilder.toString());
			ps.setString(3,termEndDateStrBuilder.toString());

			rs = ps.executeQuery();
			while(rs.next()){
				Map<String, String> data = Maps.newHashMap();
				data.put("first_coming", rs.getString(1));
				String comingTimes = rs.getString(2);
				String comingHistoryCommaText = rs.getString(3);
				if(comingHistoryCommaText != null){
					String[] comingHistoryDate = comingHistoryCommaText.split(",");
					int count = 0;					
					for (int i = 0; i < comingHistoryDate.length; i++) {
						int comingHistoryDateInt = Integer.parseInt(comingHistoryDate[i].replaceAll("/", ""));
						if(comingHistoryDateInt < repeatTermEnd){
							count++;
						}
					}
					data.put("coming_times", String.valueOf(count));				
				}
				
				modelList.add(data);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}

		return modelList;
	}
}
