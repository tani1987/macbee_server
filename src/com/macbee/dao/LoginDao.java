package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.macbee.util.DbManager;

public class LoginDao {

	private static final String SELECT_SQL = "SELECT user_id from users where user_name = ? and password = ?";

	public int login(String userName, String password) throws SQLException{

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		int userId = 0;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setString(1, userName);
			ps.setString(2, password);
			rs = ps.executeQuery();
			while(rs.next()){
				userId = rs.getInt(1);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}

		return userId;
	}
}
