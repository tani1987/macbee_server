package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.macbee.model.SalesCompareModel;
import com.macbee.model.SalesStoreModel;
import com.macbee.model.ToDoListModel;
import com.macbee.util.DbManager;

public class LoadSalesStylistDao {

	private static final String SELECT_SQL = "SELECT SUBSTRING(s.accounting_date FROM 1 For 6)" +
			", s.price, s.gender, s.division, s.nomination, s.new_or_again" +
			", c.birthday" +
			" FROM sales s, customers c" +
			" WHERE s.user_id = c.user_id AND s.user_id = ?" +
			" AND s.stylist = ?" +
			" AND c.name_kana = s.name_kana" +
			" AND ((s.name_kanji = '' AND c.name_kanji = '-  ') or (c.name_kanji = s.name_kanji))" +
			" AND s.nomination = '指名予約' AND s.accounting_date BETWEEN ? AND ? ORDER BY s.accounting_date;";

	public List<SalesStoreModel> loadData(int userId, int termStart, int termEnd, String stylist) throws SQLException{
		List<SalesStoreModel> modelList = Lists.newArrayList();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setInt(1, userId);
			ps.setString(2,stylist);
			ps.setInt(3,termStart);
			ps.setInt(4,termEnd);

			rs = ps.executeQuery();
			int currentMonth = 0;

			while(rs.next()){
				int month = rs.getInt(1);
				if(currentMonth == month){
					SalesStoreModel model = modelList.get(modelList.size() -1);
					putInfo(model, rs);
				}else{
					currentMonth = month;
					SalesStoreModel model = new SalesStoreModel();
					StringBuilder monthFormated = new StringBuilder(String.valueOf(month));
					model.setTerm(monthFormated.insert(4, "-").toString());

					putInfo(model, rs);

					modelList.add(model);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}

		return modelList;
	}

	private void putInfo(SalesStoreModel model, ResultSet rs) throws SQLException{
		int sales = rs.getInt(2);
		String gender = rs.getString(3);
		String division = rs.getString(4);
		String nomination = rs.getString(5);
		String newOrAgain = rs.getString(6);
		String birthday = rs.getString(7);

		if("技術".equals(division)){
			if("指名予約".equals(nomination)){
				model.setNominationSales(model.getNominationSales() + sales);
			}else{
				model.setFreeSales(model.getFreeSales() + sales);
			}
		}else{
			model.setGoodsSales(model.getGoodsSales() + sales);
		}
		model.setMonthlySales(model.getMonthlySales() + sales);

		if("女性".equals(gender)){
			model.setWomanCount(model.getWomanCount() + 1);
		}else{
			model.setManCount(model.getManCount() + 1);
		}
		model.setTotalCount(model.getTotalCount() + 1);

		if("新規".equals(newOrAgain)){
			model.setNewer(model.getNewer() +1);
		}else{
			model.setRepeater(model.getRepeater() + 1);
		}

		if(birthday.indexOf("-") == -1){
			int age = Integer.parseInt(birthday.replace("/", ""));
			model.setAgeTotal(model.getAgeTotal() + age);
			model.setAgeCount(model.getAgeCount() + 1);
		}
	}
}
