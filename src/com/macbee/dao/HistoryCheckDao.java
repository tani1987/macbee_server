package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.lang3.text.StrBuilder;

import com.google.common.collect.Maps;
import com.macbee.util.DbManager;

public class HistoryCheckDao {
//	private static final String SELECT_HISTORY_DATA_SQL = "SELECT customer_id, coming_date FROM histories;";
	private static final String SELECT_HISTORY_DATA_SQL = "SELECT customer_id, accounting_date FROM histories;";
	private static Map<String, String> historyDataMap;

	private static void initHistoryMap() throws SQLException {
		historyDataMap = Maps.newHashMap();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_HISTORY_DATA_SQL);
			rs = ps.executeQuery();

			while(rs.next()){
				historyDataMap.put(rs.getString(1), rs.getString(2));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps, rs);
		}
	}

	public static boolean checkData(String customerId, String accountingDate) throws SQLException {
//		if(historyDataMap == null){
//			initHistoryMap();
//		}
//		StrBuilder str = new StrBuilder(accountingDate);
//		str.insert(4, "/").insert(7,"/");
//		String comingDate = historyDataMap.get(customerId);
//		if(comingDate != null && comingDate.indexOf(str.toString()) != -1){
//			return true;
//		}else{
//			return false;
//		}
		return true;		
	}
}
