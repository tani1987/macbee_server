package com.macbee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.macbee.util.DbManager;

public class CheckToDoDao {

	private static final String UPDATE_SQL = "UPDATE histories SET attacked = ? WHERE user_id = ? AND history_id = ?;";

	public void updateStatus(int attackStatus, int userId, int historyId) throws SQLException{

		Connection conn = null;
		PreparedStatement ps = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(UPDATE_SQL);
			ps.setInt(1, attackStatus);
			ps.setInt(2, userId);
			ps.setInt(3, historyId);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
	}
}
